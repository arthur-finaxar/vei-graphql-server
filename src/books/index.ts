const { ApolloServer } = require("apollo-server");
const { MemcachedCache } = require("apollo-server-cache-memcached");
const { typeDefs } = require("./typeDefs");
const { resolvers } = require("./resolvers");

// In the most basic sense, the ApolloServer can be started
// by passing type definitions (typeDefs) and the resolvers
// responsible for fetching the data for those types.
export const booksServer = new ApolloServer({
  typeDefs,
  resolvers,
  persistedQueries: {
    cache: new MemcachedCache(
      ["memcached-server-1", "memcached-server-2", "memcached-server-3"],
      { retries: 10, retry: 10000 } // Options
    )
  },
  mocks: process.env.GRAPHQL_ENABLE_MOCKS === "true"
});

