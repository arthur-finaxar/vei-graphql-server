require("dotenv").config();
const { booksServer } = require("./books");

// This `listen` method launches a web-server.  Existing apps
// can utilize middleware options, which we'll discuss later.
booksServer.listen().then(({ url }: { url: any }) => {
  console.log(`🚀  Server ready at ${url}`);
});
