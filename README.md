# VEI GraphQL Server

## Server

1. Run Typescript watcher

`yarn run tsc:watch`

2. Run Server

`yarn start`


## Environment variables

For development, create a `.env` file and put values in these environment variables

- GRAPHQL_ENABLE_MOCKS